use axum::{
    http::{HeaderMap, Method, Uri, Version},
    Json, Router, handler::Handler, Extension,
};
use lapin::{Connection, ConnectionProperties, Channel, options::{BasicPublishOptions}, BasicProperties, publisher_confirm::Confirmation};
use serde::Serialize;

use tracing::{info};
use std::{collections::HashMap, net::SocketAddr};
use tower_http::trace::{TraceLayer, DefaultMakeSpan};

#[tokio::main()]
async fn main() {
    let amqp_addr = std::env::var("AMQP_ADDR").unwrap_or_else(|_| "amqp://localhost:5672/%2f".into());
    let conn_props = ConnectionProperties::default()
    .with_executor(tokio_executor_trait::Tokio::current())
    .with_reactor(tokio_reactor_trait::Tokio);
    let amqp_conn = Connection::connect(&amqp_addr, conn_props).await.unwrap();
    let amqp_chan = amqp_conn.create_channel().await.unwrap();      
    // let _amqp_queue = amqp_chan
    // .queue_declare(
    //     "kjm_queue",
    //     QueueDeclareOptions::default(),
    //     FieldTable::default(),
    // );

    tracing_subscriber::fmt::init();
    let app = Router::new()
        .fallback(echo_service.into_service())
        .layer(TraceLayer::new_for_http()
            .make_span_with(DefaultMakeSpan::new())
            .on_request(())
            .on_response(())//DefaultOnResponse::new().include_headers(true))
            //.on_body_chunk(new_on_body_chunk)
       )
       .layer(Extension(amqp_chan)
    );
 
    let addr = SocketAddr::from(([0, 0, 0, 0], 8888));
    tracing::debug!("listening on {}", addr);
    axum::Server::bind(&addr)
        .serve(app.into_make_service())
        .await
        .unwrap();
}

#[derive(Serialize,Debug)]
struct RequestData {
    method: String,
    version: String,
    uri: String,
    headers: HashMap<String, String>,
    body: Option<String>,
}

async fn echo_service(
    method: Method,
    uri: Uri,
    version: Version,
    headers: HeaderMap,
    body: String,
    Extension(channel): Extension<Channel>
) -> Json<RequestData> {
    let mut request_data = RequestData {
        method: method.to_string(),
        version: format!("{:?}", version),
        uri: uri.to_string(),
        headers: HashMap::new(),
        body: None,
    };
    for (name, value) in headers {
        if let Some(key) = name {
            request_data
                .headers
                .insert(key.to_string(), value.to_str().unwrap().to_string());
        }
    }
    if !body.is_empty() {
        request_data.body = Some(body);
    }
    info!("{:?}", channel.status() );
    let publish_options = BasicPublishOptions {
        immediate: false,
        mandatory: true
    };
    let publish_props = BasicProperties::default();
    let confirm = channel.basic_publish(
        "", 
        "kjm_queue", 
        publish_options,
        request_data.uri.as_bytes(),
        publish_props
    )
    .await.unwrap()
    .await.unwrap();
    info!("{:?}", confirm );
    assert_eq!(confirm, Confirmation::NotRequested);
    Json(request_data)
}
