
use std::{thread, time::Duration};

use lapin::{ConnectionProperties, Connection, options::{BasicConsumeOptions, BasicAckOptions, QueueDeclareOptions}, types::FieldTable, message::{DeliveryResult}};
use tracing::{info, error};


#[tokio::main()]
async fn main() {
    tracing_subscriber::fmt::init();
    let amqp_addr = std::env::var("AMQP_ADDR").unwrap_or_else(|_| "amqp://localhost:5672/%2f".into());
    let conn_props = ConnectionProperties::default()
    .with_executor(tokio_executor_trait::Tokio::current())
    .with_reactor(tokio_reactor_trait::Tokio);
    let amqp_conn = Connection::connect(&amqp_addr, conn_props).await.expect(&amqp_addr);
    let amqp_chan = amqp_conn.create_channel().await.unwrap();
    let _amqp_queue = amqp_chan
    .queue_declare(
        "kjm_queue",
        QueueDeclareOptions::default(),
        FieldTable::default(),
    )
    .await
    .unwrap();

    let amqp_consumer = amqp_chan.basic_consume(
        "kjm_queue", 
        "kjm_tag", 
        BasicConsumeOptions::default(),
        FieldTable::default()
    )
    .await
    .unwrap();
    amqp_consumer.set_delegate(move |delivery_res: DeliveryResult| async move {
        let delivery = match delivery_res {
            Ok(Some(delivery)) => delivery,
            Ok(None) => return,
            Err(error) => {
                dbg!("Failed to consume queue messages {}", error);
                return;
            }
        };
        delivery
            .ack(BasicAckOptions::default())
            .await
            .expect("Failed to ack message");

        info!("delivery {:?}", delivery);
        let content = delivery.data;
        info!("data {}", String::from_utf8_lossy(&content));

    });
    thread::sleep(Duration::from_secs(60*60));
}