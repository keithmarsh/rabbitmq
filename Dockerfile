FROM ubuntu:22.04
 
RUN apt-get update && apt-get install -y curl 
COPY target/debug/rabbitmq target/debug/
COPY target/debug/sub target/debug/
EXPOSE 8888
CMD target/debug/rabbitmq